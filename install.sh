#! /usr/bin/env zsh

TYPORA_THEMES=$HOME/.config/Typora/themes

echo "Syncing to $TYPORA_THEMES"
rsync -r ./ $TYPORA_THEMES

